#include "point.h"
Point::Point(float a, float b) : x(a), y(b) {};

float Point::get_x() const {return x;};
float Point::get_y() const {return y;};
