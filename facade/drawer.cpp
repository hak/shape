#include "drawer.h"
#include <SDL2/SDL_pixels.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_video.h>


Drawer::Drawer() {
  window = SDL_CreateWindow("drawer_object",0,0,600,600,SDL_WINDOW_ALLOW_HIGHDPI);  
  renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE);
  texture = SDL_CreateTexture(renderer,SDL_PIXELFORMAT_RGBA8888,SDL_TEXTUREACCESS_STATIC,600,600);
};

void Drawer::draw() {
  
};

void Drawer::show() {
  SDL_RenderPresent(renderer);
}

void Drawer::flush() {
  SDL_RenderClear(renderer);
}









