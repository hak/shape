#pragma once
#include "shape.h"

class Triangle : public Shape{
public:
  Triangle(float b, float h, int x, int y);
  float b,h;

  bool intersects(const Point &point) const override;

  float get_area() override;
};