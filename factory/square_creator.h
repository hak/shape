#pragma once
#include "square.h"
#include "factory.h"

class Square_creator : public Factory {
  Pattern *factory_method() const;
};
