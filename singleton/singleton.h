#pragma once
#include <iostream>


class Singleton { //handler

public:
  Singleton(Singleton &other) = delete;
  void operator=(const Singleton &other) = delete;

  static Singleton* get_instance(const int num); //reinstantiate it if not available

  void print_num();
  int num;


protected:
  Singleton(const int num);

private:
  static Singleton *singleton;
};





