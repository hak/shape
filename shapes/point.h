#pragma once

struct Point {
public:
  Point(float a, float b);
  float get_x() const;
  float get_y() const;

private:
  float x,y;
  
  
};
