#pragma once
#include "square.h"
#include "asterisk.h"


class Factory {
public:

  virtual ~Factory();
  virtual Pattern* factory_method() const = 0;
  
};






