#pragma once
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

class Drawer {
public:
  Drawer();
  void draw();
  void flush();
  void show();
private:
  SDL_Window * window;
  SDL_Renderer * renderer;
  SDL_Texture * texture;
  
};






