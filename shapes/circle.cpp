#include "circle.h"

float Circle::get_radius() const {return this->radius;};

Circle::Circle(float r, int a, int b) : radius(r), Shape(a, b){};
float radius;

bool Circle::intersects(const Point &point) const {
  Point dist = {point.get_x() - get_center().get_x(), point.get_y() - get_center().get_y()};
  return (dist.get_x() * dist.get_x() + dist.get_y() * dist.get_y() <= (this->radius * this->radius));
};

float Circle::get_area() { return (3.141596 * radius * radius); };
