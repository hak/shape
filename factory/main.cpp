#include "asterisk.h"
#include "square_creator.h"
#include "asterisk_creator.h"


int main() {
  Factory *factory_asterisk = new Asterisk_creator(); //instantiate the asterisk creator as a class
  Pattern *pattern = factory_asterisk->factory_method(); // now create a pattern
  pattern->print();
  pattern = factory_asterisk->factory_method(); // now create a pattern
  pattern->print();
  delete factory_asterisk;
  delete pattern;

  Factory *factory_square = new Square_creator(); //instantiate the square creator as a class
  pattern = factory_square->factory_method(); // now create a pattern
  pattern->print();
  pattern = factory_square->factory_method(); // now create a pattern
  pattern->print();
  delete factory_square;
  delete pattern;

}





