#include "singleton.h"

Singleton* Singleton::singleton = nullptr;

Singleton *Singleton::get_instance(const int num) {
  if (singleton == nullptr)
	singleton =  new Singleton(num);
  return singleton;
};

void Singleton::print_num() {std::cout << this->num << "\n";};

Singleton::Singleton(const int num) : num(num) {};





