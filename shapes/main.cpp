#include "square.h"
#include "circle.h"
#include "triangle.h"
#include "shape.h"

#include <vector>
#include <iostream>

int main() {
  std::vector<Shape*> shapeList; 
  shapeList.push_back(new Square(4, 1, 0)); // h, x, y
  shapeList.push_back(new Circle(5, 3, 6)); // r, x, y
  shapeList.push_back(new Triangle(2, 3, 0, 0)); // b, h, x, y

  Point p1 = Point(0, 0); // x, y

  printf("Square intersection with p1: %i\n", shapeList[0]->intersects(p1));
  printf("Circle intersection with p1: %i\n", shapeList[1]->intersects(p1));
  printf("Triangle intersection with p1: %i\n", shapeList[2]->intersects(p1));
  
}
