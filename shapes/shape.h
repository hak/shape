#pragma once
#include "point.h"

class Shape {
public:
  Shape(float a, float b);
  virtual bool intersects(const Point& point) const = 0;
  virtual float get_area() = 0;
  // friend ostream& operator<<(Shape& shp);
public:
  Point get_center() const;
private:
  Point center {0,0};
};
