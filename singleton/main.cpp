#include "singleton.h"


int main() {
  Singleton* s = Singleton::get_instance(3); //reassigns
  s->print_num();
  std::cout << s->num << "\n";
  s = Singleton::get_instance(2); //does not allow reassignment
  s->print_num();
  std::cout << s->num << "\n";

  delete s;
}




