#include "triangle.h"
#include <iostream>

Triangle::Triangle(float b, float h, int x, int y) : b(b), h(h), Shape(x,y) {};

bool Triangle::intersects(const Point &point) const {
    bool bottom = (point.get_y() >= get_center().get_x() - 0.5 * h);
    Point bottom_left {get_center().get_x() - 0.5f*b, get_center().get_y() - 0.5f*h };
    Point bottom_right {get_center().get_x() + 0.5f*b, get_center().get_y() - 0.5f*h };
    // printf("centre : %f, %f\n", get_center().get_x() - 0.5f*b, get_center().get_y() - 0.5f*h );
    Point bl_diff {point.get_x() - bottom_left.get_x(), point.get_y() - bottom_left.get_y()};
    Point br_diff {point.get_x() - bottom_right.get_x(), point.get_y() - bottom_right.get_y()};
    // printf("bl : %f, %f\n", bottom_left.get_x(), bottom_left.get_y());
    // printf("bl_diff : %f, %f\n", bl_diff.get_x(), bl_diff.get_y());
    bool left = (bl_diff.get_x() >= 0 && bl_diff.get_y() <= 2*h*bl_diff.get_x()/b);
    bool right = (br_diff.get_x() <= 0 && br_diff.get_y() <= 2*h*(-br_diff.get_x())/b);
    return bottom && left && right;
};

float Triangle::get_area() {
return (0.5 * b * h);
};
