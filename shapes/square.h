#pragma once
#include "shape.h"

class Square : public Shape {
public:
  Square(float h, int a, int b);

  bool intersects(const Point &point) const override;
  float get_area() override;

private:
  float h;
};
