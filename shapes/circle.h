#pragma once
#include "shape.h"

class Circle : public Shape {
public:
  Circle(float r, int a, int b);
  float get_area() override;
  float get_radius() const;

private:
  float radius;
  bool intersects(const Point &point) const override; 
};

