#include "square.h"
#include <iostream>

Square::Square(float h, int a, int b) : h(h), Shape(a, b){};
float h;

bool Square::intersects(const Point &point) const {
  Point center = this->get_center();
  // printf("%f : %f : %f\n", point.get_x() - center.get_x(), point.get_y() - center.get_y(), h/2);
  return ((-0.5 * h <= (point.get_x() - center.get_x()) && (point.get_x() - center.get_x()) <= 0.5 * h) &&
		  (-0.5 * h <= (point.get_y() - center.get_y()) && (point.get_y() - center.get_y()) <= 0.5 * h));
};

float Square::get_area() { return (h * h); };
